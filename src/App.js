import React, { Component } from "react";
import { Route, Routes } from "react-router-dom";
import Form from "./components/Form";
import Home from "./components/Home";
import About from "./components/About";
import NavigationBar from "./components/NavigationBar";
import Error from "./components/Error";

export default class App extends Component {
    render() {
        return (
            <div className="main-container">
                <NavigationBar />
                <Routes>
                    <Route exact path="/" element={<Home />} />
                    <Route exact path="/about" element={<About />} />
                    <Route exact path="/form" element={<Form />} />
                    <Route element={Error} />
                </Routes>
            </div>
        );
    }
}
