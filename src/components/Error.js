import React, { Component } from 'react'

export default class Error extends Component {
	render() {
		return (
			<div>
				<h1>Oops! Page not found!</h1>
				<h3>1. Check the link you have written and get back to My Page</h3>
				<h3>2. Check your internet connection and get back to My Page</h3>
			</div>
		)
	}
}
