import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class NavigationBar extends Component {
    render() {
        return (
            <div className="navigation-body p-3 d-flex justify-content-center">
                <Link to="./" className="navigation-home text-decoration-none">
                    {" "}
                    Home{" "}
                </Link>
                <Link to="./About" className="navigation-about text-decoration-none">
                    {" "}
                    About{" "}
                </Link>
                <Link to="./Form" className="navigation-form text-decoration-none">
                    {" "}
                    Form{" "}
                </Link>
            </div>
        );
    }
}
