import React, { Component } from "react";
import validator from "validator";
import google from "./images/google.png";
import facebook from "./images/facebook.png";
import twitter from "./images/twitter.png";

export default class Form extends Component {
    constructor() {
        super();
        this.state = {
            userName: "",
            email: "",
            age: "",
            password: "",
            confirmPassword: "",
            checkbox: false,
            terms: false,
        };
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const { email, password, age, confirmPassword, checkbox, userName } =
            this.state;

        let errors = {
            confirmPasswordError: "",
            ageError: "",
            emailError: "",
            terms: true,
            checkedError: "",
            passwordError: "",
            userNameError: "",
        };

        if (validator.isEmpty(userName)) {
            errors.userNameError = "Name should be given";
            errors.terms = false;
        }

        if (validator.isEmpty(userName) !== true) {
            const stringOfWords = userName.split(" ").join("");
            if (validator.isAlpha(stringOfWords) !== true) {
                errors.userNameError =
                    "Please enter a valid name. A valid name has characters such as [a, b, c...z] [A, B, C...Z] and space[ ]";
                errors.terms = false;
            }
        }

        if (validator.isEmpty(email)) {
            errors.emailError = "Email should be given";
            errors.terms = false;
        }

        if (
            validator.isEmpty(email) !== true &&
            validator.isEmail(email, {
                blacklisted_chars: "~ ! # $ % ^ & * ( ) _ + = -",
            }) !== true
        ) {
            errors.emailError =
                "Please enter a valid email. A valid email has characters such as [@ . a,b,c...z]";
            errors.terms = false;
        }

        if (validator.isEmpty(age)) {
            errors.ageError = "Age should be given";
            errors.terms = false;
        }

        if ((validator.isEmpty(age) !== true && age <= 10) || age >= 100) {
            errors.ageError = "Age must be atleast 10 and atmost 100";
            errors.terms = false;
        }

        if (validator.isEmpty(password)) {
            errors.passwordError = "Password should be given";
            errors.terms = false;
        }

        if (
            validator.isEmpty(password) !== true &&
            validator.isStrongPassword(password) !== true
        ) {
            errors.passwordError =
                "Password must be of length atmost 8 characters with combination of UPPERCASE, lowercase characters, Symbols and Numbers";
            errors.terms = false;
        }

        if (validator.isEmpty(confirmPassword)) {
            errors.confirmPasswordError = "Re-write the same password.";
            errors.terms = false;
        }

        if (
            password !== confirmPassword &&
            validator.isEmpty(confirmPassword) !== true
        ) {
            errors.confirmPasswordError =
                "Passwords do not match. Rewrite the password once again";
            errors.terms = false;
        }

        if (checkbox !== true) {
            errors.checkedError =
                "Agree to the Terms and Conditions by clicking the checkbox";
            errors.terms = false;
        }
        this.setState(errors);
    };

    handleChange = (event) => {
        let value = event.target.value;

        if (event.target.name === "checkbox") {
            value = event.target.checked;
        }

        this.setState({
            [event.target.name]: value,
        });
    };

    render() {
        const {
            terms,
            email,
            password,
            age,
            confirmPassword,
            checkbox,
            userName,
            userNameError,
            emailError,
            ageError,
            passwordError,
            confirmPasswordError,
            checkedError,
        } = this.state;

        return (
            <div className="forms p-5 rounded text-left">
                {terms === false ? (
                    <form
                        className="flex-column p-3 bg-white text-center rounded m-auto w-75"
                        onSubmit={this.handleSubmit}
                    >
                        <div className="big-box m-1">
                            <h1 className="heading m-1">Create Account</h1>
                            <div className="logos m-1">
                                <img
                                    src={google}
                                    className="google rounded-circle m-2"
                                ></img>
                                <img
                                    src={facebook}
                                    className="facebook m-2 rounded-circle"
                                ></img>
                                <img
                                    src={twitter}
                                    className="twitter rounded-circle m-2"
                                ></img>
                            </div>
                            <p className="info">
                                or use your email for registration:
                            </p>
                        </div>

                        <div className="form-group needs-validation">
                            <input
                                name="userName"
                                id="userName"
                                type="text"
                                value={userName}
                                placeholder="Name"
                                onChange={this.handleChange}
                                className="name box p-3 m-2 form-control"
                            ></input>
                            <div className="ml-1 userErrorClass text-danger">
                                {userNameError}
                            </div>

                            <input
                                name="age"
                                id="age"
                                value={age}
                                type="number"
                                placeholder="Age"
                                onChange={this.handleChange}
                                className="age box p-3 m-2 form-control"
                            ></input>
                            <div className="ml-1 emailErrorClass text-danger">
                                {ageError}
                            </div>

                            <input
                                name="email"
                                id="email"
                                value={email}
                                onChange={this.handleChange}
                                type="email"
                                placeholder="Email"
                                className="email box p-3 m-2 form-control"
                            ></input>
                            <div className="ml-1 ageErrorClass text-danger">
                                {emailError}
                            </div>

                            <input
                                name="password"
                                id="password"
                                value={password}
                                onChange={this.handleChange}
                                type="password"
                                placeholder="Password"
                                className="password box p-3 m-2 form-control"
                            ></input>
                            <div className="ml-1 passwordErrorClass text-danger">
                                {passwordError}
                            </div>

                            <input
                                name="confirmPassword"
                                id="confirmPassword"
                                value={confirmPassword}
                                onChange={this.handleChange}
                                type="password"
                                placeholder="Confirm Password"
                                className="password box p-3 m-2 form-control"
                            ></input>
                            <div className="ml-1 confirmPasswordErrorClass text-danger">
                                {confirmPasswordError}
                            </div>
                        </div>
                        <input
                            name="checkbox"
                            id="checkbox"
                            value={checkbox}
                            onChange={this.handleChange}
                            id="checkbox"
                            type="checkbox"
                            className="check border border-dark m-auto"
                        ></input>
                        <label
                            className="termsAndConditions"
                            htmlFor="checkbox"
                        >
                            I agree to the <span className="terms">Terms</span>{" "}
                            and <span className="privacy">Privacy Policy.</span>
                        </label>
                        <div className="my-2 text-danger">{checkedError}</div>
                        <div className="buttons my-3">
                            <button className="btn rounded signup-button">
                                Sign Up
                            </button>
                        </div>
                    </form>
                ) : (
                    <div className="text-white">
                        <h1>
                            Details of Participants who agreed to Terms and
                            Conditions are:
                        </h1>
                        <div className="details">
                            <p>Name: {userName}</p>
                            <p>Email: {email}</p>
                            <p>Age: {age}</p>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}
